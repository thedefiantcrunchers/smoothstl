To manually use the smoothing script on a single model within Blender, follow the blendersmoothingtut.pdf.



To use the automated batch smoothing script (multiple models at one time), you must have Blender (2.79 or 2.8) installed in its default installation location ("C:\Program Files\Blender Foundation\Blender", if it is not, you can edit the runsmooth.bat file with the correct location of your Blender installation). Copy/paste the runsmooth.bat and smoothSTL.py files into a dir containing the STL files you wish to smooth out.  Double click runsmooth.bat, ignore the errors displayed that read "warning DM_ensure_tessface: could not create tessfaces", they will not adversely affect the process. Wait for models to be processed!

